# ChipHorizontalList

## OverView

![alt text](https://github.com/EverK777/Images/blob/master/image_chip_lib.png?raw=true) 


---

## Configuration
### Add Module to the app
1. Download the repository
2. Import the library as module
3. Add the module to the gradle app dependecies ```implementation project(':ChipHorizontalList')```


### Add ChipHorizontalList to your xml

```
     <com.kadevjo.chiphorizontallist.ChipHorizontalList
            android:id="@+id/chipList"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginTop="10dp"
            android:fontFamily="@font/josefines"
            app:layout_constraintTop_toBottomOf="@id/toolbar"
            app:layout_constraintStart_toStartOf="parent"
            app:textColorChip="@color/white"
            app:textSizeChip="14"
            app:backgroundColorChip="@color/purple_200"/>
```
			
### Custom properties

* ```  app:textColorChip="@color/white" ``` The text color of the chips
* ```  app:textSizeChip="14" ``` The text size of the chips(don't use sp the lib convert the value to sp automatically)
* ```  app:backgroundColorChip="@color/purple_200" ``` The backgroundColor of the chips
* ```  android:fontFamily="@font/josefines" ``` Font Family of the text

### Set data

```
 val list : ArrayList<ChipEntity> = ArrayList()
 chipList.setEntries(list)

```

### Set data using dataBinding

#### Create BindingAdapter
```
@BindingAdapter("setData")
fun setData(chipHorizontalList: ChipHorizontalList, entries: ArrayList<ChipEntity>){
    chipHorizontalList.setEntries(entries)
}
```


#### Add the property to the xml file

```
    <data>
        <variable
            name="viewModel"
            type="com.kadevjo.android.DataBindingExampleViewModel" />

    </data>

 <com.kadevjo.chiphorizontallist.ChipHorizontalList
            android:id="@+id/chipList"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginTop="10dp"
            android:fontFamily="@font/josefines"
            app:layout_constraintTop_toBottomOf="@id/toolbar"
            app:layout_constraintStart_toStartOf="parent"
            app:textColorChip="@color/white"
            app:textSizeChip="14"
            app:backgroundColorChip="@color/purple_200"
            app:setData="@{viewModel.chipsList}"/>

```

#### Add the LiveData to the ViewModel

```
class DataBindingExampleViewModel : ViewModel() {

    val chipsList: MutableLiveData<ArrayList<ChipEntity>> = MutableLiveData()
}	
	
```

### Listener of item click
```
 binding.chipList.setOnChipClickListener() { item, position ->

            Snackbar.make(binding.mainView, "Item ${item.text} clicked at position $position", Snackbar.LENGTH_LONG)
                .show()

 }
```

