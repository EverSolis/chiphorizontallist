package com.kadevjo.chiphorizontallist


interface OnClickListener {
    /**
     * Is triggered when a ship is clicked
     * @param item the [ChipEntity] item that was clicked
     * @param recyclerPosition the position of the RecyclerView Adapter
     */
    fun onClick (item : ChipEntity, recyclerPosition : Int)
}