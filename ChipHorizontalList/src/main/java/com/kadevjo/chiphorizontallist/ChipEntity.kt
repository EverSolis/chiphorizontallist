package com.kadevjo.chiphorizontallist

import androidx.annotation.DrawableRes

/**
 * Entity that fills the recyclerView
 */
data class ChipEntity(
    val text : String,
    @DrawableRes val icon : Int
)