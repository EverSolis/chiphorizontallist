package com.kadevjo.chiphorizontallist

/**
 * @author Ever Antonio Morales Solis
 */

import android.annotation.TargetApi
import android.content.Context
import android.graphics.PorterDuff
import android.os.Build
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import kotlinx.android.synthetic.main.container_view.view.*
import kotlinx.android.synthetic.main.item_chip_view.view.*


/**
 * Custom View that creates a list of Chips
 * @property hasAnimation if the Chips apply an animation when they show
 * @property clickListener the lister that allows to check if a item was clicked
 * @property entries the data that is going to be used to populate the RecyclerView
 * @property adapter the [DynamicAdapter] that is going to be used to set the Recyclerview
 */

class ChipHorizontalList : RelativeLayout {

    private var hasAnimation = false
    private var clickListener: com.kadevjo.chiphorizontallist.OnClickListener? = null
    private var entries: ArrayList<ChipEntity> = ArrayList()
    private var adapter: DynamicAdapter<ChipEntity>? = null


    @JvmOverloads
    constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0,
    ) : super(context, attrs, defStyleAttr) {
        config(attrs)

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int,
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        config(attrs)
    }


    private fun config(attrs: AttributeSet?) {
        LayoutInflater.from(context).inflate(R.layout.container_view, this, true)
        attrs.let {
            val typedArray = context.obtainStyledAttributes(
                it,
                R.styleable.ChipHorizontalList,
                0,
                0
            )

            val backgroundColorChip: Int =
                typedArray.getColor(
                    R.styleable.ChipHorizontalList_backgroundColorChip,
                    ContextCompat.getColor(context, R.color.defBackground))

            val textColorChip =
                typedArray.getColor(
                    R.styleable.ChipHorizontalList_textColorChip,
                    ContextCompat.getColor(context, R.color.defTextColor))

            var fontFamilyChip: Int? = null
            val fontFamilyId =
                typedArray.getResourceId(R.styleable.ChipHorizontalList_android_fontFamily,
                    0)
            if (fontFamilyId > 0) {
                fontFamilyChip = fontFamilyId
            }


            val textSizeChip =
                typedArray.getFloat(
                    R.styleable.ChipHorizontalList_textSizeChip,
                    10f)

            configRecyclerView(
                backgroundColorChip = backgroundColorChip,
                textColorChip = textColorChip,
                fontFamilyChip = fontFamilyChip,
                textSizeChip = textSizeChip)


            typedArray.recycle()

        }
    }

    /**
     * Config the [DynamicAdapter] for the RecyclerView
     * @param backgroundColorChip the color of the background of the Chip
     * @param textColorChip the text color of the Chip
     * @param fontFamilyChip the font family that is going to have the text of the Chip
     * @param textSizeChip the size of the text that is going to have the Chip
     */
    private fun configRecyclerView(
        backgroundColorChip: Int,
        textColorChip: Int,
        fontFamilyChip: Int?,
        textSizeChip: Float,
    ) {

        adapter = DynamicAdapter(
            layout = R.layout.item_chip_view,
            entries = entries,
            action = fun(_, view, item, position) {
                view.icon_feature.setImageResource(item.icon)
                view.mainView.background.setColorFilter(
                    backgroundColorChip,
                    PorterDuff.Mode.SRC_IN
                )
                view.feature_text.apply {
                    text = item.text
                    setTextColor(textColorChip)

                    if (fontFamilyChip != null) {
                        val face = ResourcesCompat.getFont(context, fontFamilyChip)
                        typeface = face
                    }

                    setTextSize(TypedValue.COMPLEX_UNIT_SP, textSizeChip)
                }

                view.setOnClickListener {
                    if (this.clickListener != null) {
                        this.clickListener?.onClick(item, position)
                    }
                }
            })

        recyclerViewChip.adapter = adapter

    }

    /**
     * Set the data to the List
     * @param data the data that has be used to populate the recyclerview
     */

    fun setEntries(data: ArrayList<ChipEntity>) {
        entries.addAll(data)
        adapter?.notifyDataSetChanged()
    }

    /**
     * Listener that is triggered when a Chip is clicked
     * @param onClick lambda that contains the  element and the adapter position
     */
    fun setOnChipClickListener(onClick: (item: ChipEntity, position: Int) -> Unit) {
      this.clickListener = object : com.kadevjo.chiphorizontallist.OnClickListener{
            override fun onClick(item: ChipEntity, recyclerPosition: Int) {
                onClick.invoke(item, recyclerPosition)
            }
        }
    }
}

