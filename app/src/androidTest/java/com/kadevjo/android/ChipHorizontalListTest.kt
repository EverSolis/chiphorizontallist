package com.kadevjo.android

import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.kadevjo.chiphorizontallist.DynamicAdapter
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class ChipHorizontalListTest {



    @get:Rule
    val activityRule = ActivityTestRule(DataBindingExampleActivity::class.java)


    @Test
    fun testViewHasData() {
        restartActivity()
        onView(withId(R.id.recyclerViewChip))
            .check(matches(atPosition(0, hasDescendant(withText("Beach")))))
    }

    @Test
    fun checkItemClick(){
        onView(withId(R.id.recyclerViewChip))
            .perform(RecyclerViewActions
                .actionOnItemAtPosition<DynamicAdapter.ViewHolder>(0, click()))

        onView(withId(com.google.android.material.R.id.snackbar_text))
            .check(matches(withText("Item Beach clicked at position 0")))
    }



    private fun restartActivity() {
        if (activityRule.activity != null) {
            activityRule.finishActivity()
        }
        activityRule.launchActivity(Intent())
    }

    private fun atPosition(position: Int, itemMatcher: Matcher<View?>): Matcher<View?>? {
        return object : BoundedMatcher<View?, RecyclerView>(RecyclerView::class.java) {
            override fun describeTo(description: Description) {
                description.appendText("has item at position $position: ")
                itemMatcher.describeTo(description)
            }

            override fun matchesSafely(view: RecyclerView): Boolean {
                val viewHolder = view.findViewHolderForAdapterPosition(position)
                    ?: // has no item on such position
                    return false
                return itemMatcher.matches(viewHolder.itemView)
            }
        }
    }


}