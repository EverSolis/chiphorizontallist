package com.kadevjo.android

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import com.google.android.material.snackbar.Snackbar
import com.kadevjo.android.databinding.ActivityDataBindingExampleBinding
import com.kadevjo.chiphorizontallist.ChipEntity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DataBindingExampleActivity : AppCompatActivity() {

    private lateinit var binding : ActivityDataBindingExampleBinding
    private val viewModel: DataBindingExampleViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDataBindingExampleBinding.inflate(layoutInflater)
        val view = binding.root
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        configViewActions(binding)
        val list : ArrayList<ChipEntity> = ArrayList()
        binding.chipList.setEntries(list)
        setContentView(view)
    }

    private fun configViewActions(binding: ActivityDataBindingExampleBinding) {
        binding.chipList.setOnChipClickListener() { item, position ->

            Snackbar.make(binding.mainView, "Item ${item.text} clicked at position $position", Snackbar.LENGTH_LONG)
                .show()

        }
    }
}