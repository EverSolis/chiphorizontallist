package com.kadevjo.android

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kadevjo.chiphorizontallist.ChipEntity

class DataBindingExampleViewModel : ViewModel() {

    val chipsList: MutableLiveData<ArrayList<ChipEntity>> = MutableLiveData()

    init {
        chipsList.value = loadData()
    }

    private fun loadData() : ArrayList<ChipEntity>{
        val list : ArrayList<ChipEntity> = ArrayList()

        list.add(ChipEntity(text = "Beach", R.drawable.ic_baseline_beach_access_24))
        list.add(ChipEntity(text = "Bike", R.drawable.ic_baseline_bike_scooter_24))
        list.add(ChipEntity(text = "Construction", R.drawable.ic_baseline_construction_24))
        list.add(ChipEntity(text = "Corona virus", R.drawable.ic_baseline_coronavirus_24))
        list.add(ChipEntity(text = "Step", R.drawable.ic_baseline_do_not_step_24))
        list.add(ChipEntity(text = "Touch", R.drawable.ic_baseline_do_not_touch_24))
        list.add(ChipEntity(text = "Eco", R.drawable.ic_baseline_eco_24))

        return list
    }


}