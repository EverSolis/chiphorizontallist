package com.kadevjo.android

import androidx.databinding.BindingAdapter
import com.kadevjo.chiphorizontallist.ChipEntity
import com.kadevjo.chiphorizontallist.ChipHorizontalList

@BindingAdapter("setData")
fun setData(chipHorizontalList: ChipHorizontalList, entries: ArrayList<ChipEntity>){
    chipHorizontalList.setEntries(entries)
}